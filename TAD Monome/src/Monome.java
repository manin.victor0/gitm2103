public class Monome {

    /**
     * coefficient du mon�me
     */
    private float coefficient;

    /**
     * exposant du mon�me
     */
    private int exposant;

    /**
     * construit un mon�me
     * 
     * @param coefficient
     *            coefficient du mon�me
     * @param exposant
     *            exposant du mon�me
     * @exception IllegalArgumentException
     *                si l'exposant est n�gatif
     */
    public Monome(float coefficient, int exposant)
            throws IllegalArgumentException {
    	if (exposant < 0){
    		throw new IllegalArgumentException("exposant n�gatif" + exposant);
    	}
    	this.coefficient = coefficient;
    	this.exposant = exposant;
    }

    /**
     * retourne le coefficient d'un mon�me
     * 
     * @return coefficient
     */
    public float getCoefficient() {
        return this.coefficient;
    }

    /**
     * retourne l'exposant d'un mon�me
     * 
     * @return exposant
     */
    public int getExposant() {
        return this.exposant;
    }

    /**
     * calcule la somme de deux mon�mes
     * 
     * @param m
     *            deuxi�me op�rande de la somme
     * @return mon�me r�sultat
     * @exception ArithmeticException
     *                si les exposants des 2 mon�mes ne sont pas les m�mes
     */
    public Monome somme(Monome m) throws ArithmeticException {
        if (this.getExposant() != m.getExposant()) {
        	throw new ArithmeticException("exposant diff�rents" + this.getExposant() + m.getExposant());
        }
        return new Monome(this.getCoefficient() + m.getCoefficient(), this.getExposant());
    }

    /**
     * calcule le produit de deux mon�mes
     * 
     * @param m
     *            deuxi�me op�rande du produit
     * @return mon�me r�sultat
     */
    public Monome produit(Monome m) {
        return new Monome (this.getCoefficient() * m.getCoefficient(), this.getExposant() + m.getExposant());
    }

    /**
     * calcule la d�riv�e d'un mon�me
     * 
     * @return mon�me r�sultat
     */
    public Monome d�riv�e() {
        if (this.getExposant() == 0) {
        	return new Monome(0.0F, 0);
        } else {
        	return new Monome(this.getCoefficient() * this.getExposant(), this.getExposant() - 1);
        }
    }

    /**
     * produit une version unicode d'un mon�me
     * 
     * @return cha�ne r�sultat
     */
    @Override
    public String toString() {
        if (this.estNul()) {
            return "0";
        } else {
            if (this.exposant == 0) {
                return "" + this.getCoefficient();
            }
            return "" + this.getCoefficient() + "xe" + this.getExposant();
        }
    }

    /**
     * teste si un mon�me est nul
     * 
     * @return true si le mon�me est nul
     */
    public boolean estNul() {
        if (this.getCoefficient() == 0.0 ) {
        	return true;
        } else {
        	return false;
        }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(coefficient);
		result = prime * result + exposant;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// Teste si les deux objets � comparer ont la m�me adresse m�moire
		if (this == obj)
			return true;
		// Teste si le param�tre fant�me est null, cad pas allou� (new)
		if (obj == null)
			return false;		
		// Teste si les deux objets appartiennent � la m�me classe
		if (getClass() != obj.getClass())
			return false;
		// Teste le contenu, cad les valeurs des variables d'instances
		// Transtype obj en Monome
		Monome other = (Monome) obj;
		// compare le coefficient des deux mon�mes
		if (Float.floatToIntBits(coefficient) != Float.floatToIntBits(other.coefficient))
			return false;
		// compare l'exposant des 2 mon�mes
		if (exposant != other.exposant)
			return false;
		// si tous les tests sont pass�s, alors les deux objets sont �gaux
		return true;
	}

}
