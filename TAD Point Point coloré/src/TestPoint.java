import static org.junit.Assert.*;

import org.junit.Test;

public class TestPoint {

	@Test
	public void testPoint() {
		Point p = new Point(0,0);
		assertEquals(0, p.getAbscisse(), 0);
		assertEquals(0, p.getOrdonnée(), 0);
	}

	@Test
	public void testGetAbscisse() {
		Point p = new Point(5,3);
		assertEquals(5, p.getAbscisse(), 0);
	}

	@Test
	public void testGetOrdonnée() {
		Point p = new Point(5,3);
		assertEquals(3, p.getOrdonnée(), 0);
	}

	@Test
	public void testTranslater() {
		Point p = new Point(5,3);
		p.translater(3, 2);
		assertEquals(8, p.getAbscisse(), 0);
		assertEquals(5, p.getOrdonnée(), 0);
	}

	@Test
	public void testMettreAEchelle() {
		Point p = new Point(5,3);
		p.mettreAEchelle(2);
		assertEquals(10, p.getAbscisse(), 0);
		assertEquals(6, p.getOrdonnée(), 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
		public void testExMettrreAEchelle() {
		Point p = new Point(5,3);
		p.mettreAEchelle(-3);
	}

}
