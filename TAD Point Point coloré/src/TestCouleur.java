import static org.junit.Assert.*;

import org.junit.Test;

public class TestCouleur {

	@Test
	public void testCouleur() {
		Couleur c = new Couleur(0 ,0 ,0);
		assertEquals(0, c.getRouge());
		assertEquals(0, c.getBleu());
		assertEquals(0, c.getVert());
		
	}

	@Test
	public void testGetRouge() {
		Couleur c = new Couleur(30, 0, 0);
		assertEquals(30, c.getRouge());
	}

	@Test
	public void testGetVert() {
		Couleur c = new Couleur(0, 30, 0);
		assertEquals(30, c.getVert());
	}

	@Test
	public void testGetBleu() {
		Couleur c = new Couleur(0, 0, 30);
		assertEquals(30, c.getBleu());
	}

	@Test
	public void testValeurRVB() {
		Couleur c = new Couleur(50, 50, 50);
		assertEquals(3289650, c.valeurRVB());
	}

	@Test
	public void testSetRouge() {
		Couleur c = new Couleur(50, 50, 50);
		c.setRouge(20);
		assertEquals(20, c.getRouge());
	}

	@Test
	public void testSetVert() {
		Couleur c = new Couleur(50, 50, 50);
		c.setVert(20);
		assertEquals(20, c.getVert());
	}

	@Test
	public void testSetBleu() {
		Couleur c = new Couleur(50, 50, 50);
		c.setBleu(20);
		assertEquals(20, c.getBleu());
	}
	@Test(expected = IllegalArgumentException.class)
		public void testExSetRouge() {
		Couleur c = new Couleur(50, 50, 50);
		c.setRouge(256);
		}
	@Test(expected = IllegalArgumentException.class)
		public void testExSetVert() {
		Couleur c = new Couleur(50, 50, 50);
		c.setVert(256);
	}
	@Test(expected = IllegalArgumentException.class)
		public void testExSetBleu() {
		Couleur c = new Couleur(50, 50, 50);
		c.setBleu(369);
	}
}



