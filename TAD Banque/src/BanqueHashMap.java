import java.util.LinkedList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class BanqueHashMap {
	
	private String libelle;
	
	private Map<String, CompteBancaire> comptes;
	
	public Banque(String libelle) {
		this.libelle = libelle;
		this.comptes = new HashMap<>();
	}
	
	public String getLibelle() {
		return this.libelle;
	}
	
	private CompteBancaire getCompte(String numero) {
		return this.comptes.get(numero);
		}
	
	
	public void ouvrir(String numero, float valeur) {
		if (this.comptes.containsKey(numero)) {
			throw new IllegalArgumentException("Compte bancaire de num�ro "+ numero +" d�j� pr�sent");
		}
		CompteBancaire c = new CompteBancaire(numero);
		c.d�poser(valeur);
		this.comptes.put(numero, c);
		
	}
	public void fermer(String num�ro) {
		if(!this.comptes.containsKey(num�ro)) {
			throw new IllegalArgumentException ("compte bancaire de num�ro" + num�ro + " non pr�sent dans la banque");
		}
		if (this.comptes.get(num�ro).solde() != 0.0F) {
			throw new IllegalArgumentException("compte bancaire de numero " + num�ro + "non vide. il ne peut pas �tre ferm�");
		}
		if (this.comptes.get(num�ro).solde() == 0.0F) {
			this.comptes.remove(num�ro);
		}
	}
	
	public void d�poser (String numero, float valeur) {
		CompteBancaire compteACrediter = this.getCompte(numero);
		compteACrediter.d�poser(valeur);
	}

	
	public float solde(String num�ro) {
		return this.getCompte(num�ro).solde();
	}
	
	public Boolean estCompteExistant(String numero){ 
		for(CompteBancaire compteCourant : this.comptes) {
			
		}
		
	}
	
	@Override
	public String toString() {
		String ch ="[Banque : " + libelle;
		Collection<CompteBancaire> listeComptes = this.comptes.values();
		for (CompteBancaire c : listeComptes) {
			ch += "\n" + c;
			return ch;
		}
	}
}