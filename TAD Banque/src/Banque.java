import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.junit.platform.commons.util.ToStringBuilder;


public class Banque {
	
	private String libelle;
	
	private List<CompteBancaire> comptes;
	
	public  Banque(String l) {
		this.libelle = l;
		this.comptes = new LinkedList<>();
	}
	
	public String getLibelle() {
		return this.libelle;
	}
	
	private CompteBancaire getCompte(String numero) {
		for (CompteBancaire c : this.comptes) {
			String num�roCompteCourant = c.getNum�ro();	
			if(num�roCompteCourant.equals(numero)) {
				return c;
			}
		}
		return null;
	}
	
	public void ouvrir(String numero, float valeur) {
		if (this.estCompteExistant(numero)) {
			throw new IllegalArgumentException("Le compte existe d�j�");
		}
		CompteBancaire compteACreer = new CompteBancaire(numero);
		this.comptes.add(compteACreer);
		compteACreer.d�poser(valeur);
		
	}
	public void fermer(String num�ro) {
		if(!this.estCompteExistant(num�ro)) {
			throw new IllegalArgumentException ("compte bancaire de num�ro" + num�ro + " non pr�sent dans la banque");
		}
		if (this.getCompte(num�ro).solde() != 0.0F) {
			throw new IllegalArgumentException("compte bancaire de numero " + num�ro + "non vide. il ne peut pas �tre ferm�");
		} else {
			this.comptes.remove(getCompte(num�ro));
		}
		
	}
	
	public void d�poser (String numero, float valeur) {
		CompteBancaire compteACrediter = this.getCompte(numero);
		compteACrediter.d�poser(valeur);
	}
	
	public void retirer(String numero, float valeur) {
		CompteBancaire compteADebiter = this.getCompte(numero);
		compteADebiter.retirer(valeur);
	}
	
	public float solde(String num�ro) {
		return this.getCompte(num�ro).solde();
	}
	
	public Boolean estCompteExistant(String numero){ 
		for (CompteBancaire compteCourant : this.comptes) {
			String numeroCompteCourant = compteCourant.getNum�ro();
			if(numeroCompteCourant.equals(numero)) {
				return true;
			}
		}
		return false;
	}
	
	
}
