import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CompteBancaireTest {
	private CompteBancaire c1;
	private CompteBancaire c2;
	
	@Before
	public void setUp() throws Exception {
		c1 = new CompteBancaire ("azer");
	}
	

	@Test
	public void testEqualsObject01() {
		c2 = new CompteBancaire ("azer");
		
		assertFalse (c1 == c2);
		assertTrue(c1.equals(c2));
	}
	@Test
	public void testEqualsObject02() {
		c2 = new CompteBancaire ("azzr");
		
		assertFalse (c1 == c2);
		assertFalse(c1.equals(c2));
	}
	
	@Test
	public void testEqualsObject03() {
		c2 = null;
		
		assertFalse (c1 == c2);
		assertFalse(c1.equals(c2));
	}
	
	@Test
	public void testEqualsObject04() {
		c2 = c1;
		assertTrue(c1 == c2);
		assertTrue (c1.equals(c2));
	}
	@After
	public void tearDown() throws Exception {
		c1 = null;
		c2 = null;
	}
}
